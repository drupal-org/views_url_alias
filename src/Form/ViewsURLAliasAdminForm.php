<?php

namespace Drupal\views_url_alias\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides URL alias form for admin.
 */
class ViewsURLAliasAdminForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'views_url_alias_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): string {
    return t('Are you sure you want to rebuild the Views URL alias table?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('views_ui.settings_basic');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return t('This should only be needed if URL aliases have been updated outside the URL alias edit form.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): string {
    return t('Rebuild table');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    views_url_alias_rebuild();
    $form_state->setRedirectUrl(new Url('views_ui.settings_basic'));
  }

}
